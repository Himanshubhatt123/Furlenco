package org.furlenco.student.classes;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentOperationClass
{	
	
	@Autowired
	private StudentService service;
	
	
	@RequestMapping("/allstudent")
	public List<Student> getallstudent()
		{ 
		return  service.getallStudent();
		}
	
	@RequestMapping("/allstudent/{id}")
	public Optional<Student> getStudent(@PathVariable Long id)
	{
		return service.getStudent(id); 
	}
	
	
	@RequestMapping(method = RequestMethod.POST, value ="/allstudent")
	public void addStudent(@RequestBody Student newstudent)
	{
		service.addStudent(newstudent);
	}
	
	@RequestMapping(method = RequestMethod.PATCH, value ="/allstudent/{id}")
	public void updateStudent(@RequestBody Student updatestudent, @PathVariable String id) 
	{
	     
	    service.updateStudent(id ,updatestudent);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value ="/allstudent/{id}")
	public void deleteStudent( @PathVariable Long id) 
	{
		service.deleteStudent(id);
	}
}
