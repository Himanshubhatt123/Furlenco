package org.furlenco.student.classes;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Transactional
@Repository
public interface StudentRepository extends CrudRepository<Student , Long>
{

	public List<Student> findAll();
	public Student findByid(Long id);
	
}

