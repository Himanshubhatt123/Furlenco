package org.furlenco.student.classes;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Student")
public class Student implements Serializable
{

	private static final long serialVersionUID = -3009157732242241606L;
	
@Id

 private long id;

 private String name;

 private int standard;

 private boolean active;

private Date created;
 
public Student() {}

public Student(long id,String name, int standard, boolean active) 
{
	super();
	this.id = id;
	this.name = name;
	this.standard = standard;
	this.active = active;
	created = new Date();
}
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@NotNull
@Column(name="S_id" , unique = true )
public long getid() {
	return id;
}


public void setid(long id) {
	this.id = id;
}


@Column(name="S_name")
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}

@Column(name="S_Standard")
public int getStandard() {
	return standard;
}
public void setStandard(int standard) {
	this.standard = standard;
}


public boolean isActive() {
	return active;
}

@Column(name="Active")
public void setActive(boolean active) {
	this.active = active;
}

@NotNull

public Date getCreated() {
	return created;
}

public void setCreated(Date created) {
	this.created = created;

}

@Override
public String toString() {
	return String.format("Student[id=%d, name='%s', standard='%s' , active = '%b', ]", id, name, standard , active );
}
}
