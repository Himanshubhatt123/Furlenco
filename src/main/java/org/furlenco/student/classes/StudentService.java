package org.furlenco.student.classes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentService 
{
	
	@Autowired
	private StudentRepository repository;
	
	public List<Student> getallStudent()
	{
		List<Student> student = new ArrayList<>();
		repository.findAll().forEach(student::add);
		return student;
	}
	
	public Optional<Student> getStudent(Long id)
	{
		return repository.findById(id);
	}

	public void updateStudent(String id, Student updatestudent) 
	{
		repository.save(updatestudent);
	}
	
	public void addStudent(Student newstudent)
	{
		repository.save(newstudent);
	}
	
	public void deleteStudent(Long id) 
	{
		repository.deleteById(id);
	}
}
